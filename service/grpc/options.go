package grpc

import (
	"crypto/tls"

	gc "gitlab.com/diycoder/go-micro/client/grpc"
	gs "gitlab.com/diycoder/go-micro/server/grpc"
	"gitlab.com/diycoder/go-micro/service"
)

// WithTLS sets the TLS config for the service
func WithTLS(t *tls.Config) service.Option {
	return func(o *service.Options) {
		o.Client.Init(
			gc.AuthTLS(t),
		)
		o.Server.Init(
			gs.AuthTLS(t),
		)
	}
}

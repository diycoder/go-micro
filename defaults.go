package micro

import (
	"gitlab.com/diycoder/go-micro/client"
	"gitlab.com/diycoder/go-micro/debug/trace"
	"gitlab.com/diycoder/go-micro/server"
	"gitlab.com/diycoder/go-micro/store"

	// set defaults
	gcli "gitlab.com/diycoder/go-micro/client/grpc"
	memTrace "gitlab.com/diycoder/go-micro/debug/trace/memory"
	gsrv "gitlab.com/diycoder/go-micro/server/grpc"
	memoryStore "gitlab.com/diycoder/go-micro/store/memory"
)

func init() {
	// default client
	client.DefaultClient = gcli.NewClient()
	// default server
	server.DefaultServer = gsrv.NewServer()
	// default store
	store.DefaultStore = memoryStore.NewStore()
	// set default trace
	trace.DefaultTracer = memTrace.NewTracer()
}
